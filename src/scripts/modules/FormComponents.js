export class Select {
    constructor() {

    }

    render() {
        return `<div class="form-group">
                    <lable for="urgency">Select urgency</lable>
                    <select class="form-control visit-form-input" id="urgency" name="urgency">
                        <option value="normal">Normal</option>
                        <option value="priority">Priority</option>
                        <option value="urgent">Urgent</option>
                    </select>
                </div>`
    };

};


export class Input {
    constructor(props) {

        for (let key in props) {
            this[key] = props[key];
        }
    }

    render() {
        return Object.entries(this)
            .map(([key, value]) => {
                return `<div class="form-group">
                            <lable for="${key}">${value}</lable>
                            <input class="form-control visit-form-input" id="${key}" name="${key}">
                        </div>`
            }).join('');
    }
    static clearInput(input){
        input.value = '';
    }
}

export class Button {
    constructor() {
    }
    static createVisitButton(){
        let button = document.createElement('button');
        button.id = 'createVisit';
        button.className = 'btn btn-primary';
        button.setAttribute('data-target', '#createVisitModal');
        button.setAttribute('data-toggle', 'modal');
        button.innerText = 'Create Visit';
        return button
    }
}