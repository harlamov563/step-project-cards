let authorisationUrl = 'https://ajax.test-danit.com/api/cards/login/';

export class User {
    constructor(email, password) {
        this.email = document.getElementById('exampleInputEmail1').value;
        this.password = document.getElementById('exampleInputPassword1').value;

        }
    async getToken () {
        let response = await fetch(authorisationUrl, {
            method: 'POST',
            body: JSON.stringify(new User()),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        // console.log(response);
        if (response.ok === false) {
            return alert('Email or password is incorrect!')
        } else {
            alert('Authorisation approved!');
            // console.log(await response.text());
            return await response.text();
        }
    }
}