
let token = localStorage.getItem('token');

export class Card {
    constructor() {
    }

    static async getAllCards() {

        const response = await fetch(`https://ajax.test-danit.com/api/cards`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        return await response.json();
    }

    static async deleteCard(object) {
        const response = await fetch(`https://ajax.test-danit.com/api/cards/${object.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        return true
    }

    static createCard(object) {
        let cardID = object.id;
        let {content} = object;
        let {doctor, ...rest} = content;
        console.log(content);

        return `
                <p class="card-id">Visit # ${cardID}</p>
                <button type="button" data-action="delete" class="btn btn-danger" id="deleteBtn">DELETE</button>
                <button type="button" data-action="edit" class="btn btn-warning" id="editBtn">EDIT</button>
                <button type="button" style="display: none" data-action="save" class="btn btn-success" id="saveBtn">SAVE</button>
                <button type="button" data-action="show" class="btn btn-primary" id="toggleBtn">SHOW</button>
                <div class="info-hidden">
                ${Object.entries({doctor, ...rest})
            .map(([key, value]) =>
                `
                ${key}  <input class="card-input" type="text" name="${key}" value="${value}" readonly="true">`).join('')}
                </div>`
    }

    // static editCard(card){
    //     let editedContent = {}
    //     let allCardNodes = card.closest('div').childNodes;
    //     let id = allCardNodes[1].textContent.slice(8, 12);
    //     let content = allCardNodes[allCardNodes.length - 1];
    //     let inputs = content.children.removeAttribute('readonly');
    //     // inputs.removeAttribute('readonly');
    //     for (let element of inputs){
    //         editedContent[element.name] = element.value;
    //     }
    //     console.log(editedContent);
    //     const editedCard = {'id': id, 'content': editedContent};
    //     console.log(editedCard);
    // }
}