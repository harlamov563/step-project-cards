export class Ajax {
    constructor() {
    }

    static async postRequest (object, url, token) {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(object),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        return response.json();
    }
    static async putRequest (object, cardId){
        const response = await fetch(`https://ajax.test-danit.com/api/cards/${cardId}`, {
            method: 'PUT',
            body: JSON.stringify(object),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        return response.json();
    }
}