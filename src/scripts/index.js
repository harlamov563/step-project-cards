import {Button} from "./modules/FormComponents.js";
import {Input} from "./modules/FormComponents.js";
import {Ajax} from "./modules/ClassAjax.js";

const modalForm = document.getElementById('formContent');
modalForm.addEventListener('click', createVisitOnBtnClick);

/**
 *@Desc Function dynamically adds inputs based on the selected doctor.
 **/
async function createVisitOnBtnClick(event) {
    const {RenderByDoctor} = await import('./modules/ClassRenderByDoctor.js');
    const {CreateVisitForm} = await import('./modules/CreateVisitForm.js');
    const {target: doctor} = event
    const wrapper = document.getElementById('wrapper');
    const modal = document.getElementById('createVisitModal');

    if (doctor.value === 'dentist' || doctor.value === 'therapist' || doctor.value === 'cardiologist') {
        new RenderByDoctor(doctor.value).render(wrapper);
    } else if (!modal.classList.contains('show')) {
        new CreateVisitForm().clearInputs('wrapper');
    }

};

const submitVisitFormBtn = document.getElementById('createVisitBtn');

/**
 *@Desc Function sends form data to the server.
 **/
async function visitFormOnSubmit() {
    const {CreateVisitForm} = await import('./modules/CreateVisitForm.js');
    const card = await new CreateVisitForm().getObj('visit-form-input');
    const response = await new CreateVisitForm().formSubmit(card);

    console.log('Response --->', response);
    return response;
};

let email = document.getElementById('exampleInputEmail1');
let password = document.getElementById('exampleInputPassword1');

/*On button 'close' the inputs are cleared*/

let closeButton = document.getElementById('closeButton');

closeButton.addEventListener('click', function () {
    Input.clearInput(email);
    Input.clearInput(password);
})

/*On button 'X' the inputs are cleared*/

let closeIconButton = document.getElementById('closeIconButton');

closeIconButton.addEventListener('click', function () {
    Input.clearInput(email);
    Input.clearInput(password);
})

/*The whole authorisation*/
import {User} from "./modules/classAuthorisation.js";

let authoriseButton = document.getElementById('authorise');
let allCardsContainer = document.getElementById('allCardsContainer');

authoriseButton.addEventListener('click', async function (event) {
    let token = await new User().getToken();
    if (token !== undefined) {
        localStorage.setItem('token', token);
        console.log('token -->>', token)
        document.getElementById('logInHtml').remove();
        let button = Button.createVisitButton();
        document.getElementById('placeForCreateVisitButton').append(button);
        /*Doesn't work*/

        let allCardsFromDataBase = Card.getAllCards();
        allCardsFromDataBase.then(function (array) {
            if (array.length !== 0) {
                document.getElementById('noItemsAdded').classList.add('disable');
                array.forEach(function (element) {
                    console.log(element);
                    let cardContainer = document.createElement('div');
                    cardContainer.classList.add('card-container');
                    allCardsContainer.appendChild(cardContainer);
                    cardContainer.innerHTML = `${Card.createCard(element)}`;
                    const deleteBtn = document.getElementById('deleteBtn');
                    deleteBtn.addEventListener('click', function () {
                        return Card.deleteCard(element);
                    })
                })
            } else {
                document.getElementById('noItemsAdded').classList.remove('disable');
            }
        })
    }
})

/*Creation of a card*/
import {Card} from "./modules/ClassCard.js";

submitVisitFormBtn.addEventListener('click', async function () {
    let cardContainer = document.createElement('div');
    cardContainer.classList.add('card-container');
    allCardsContainer.appendChild(cardContainer);
    let card = await visitFormOnSubmit();
    cardContainer.innerHTML = `${Card.createCard(card)}`;

    /*Delete the card from the Database*/

    const deleteBtn = document.getElementById('deleteBtn');
    deleteBtn.addEventListener('click', async function () {
        return await Card.deleteCard(card);
    })
})
/*Get the text 'no items added'*/

// if (allCards.hasChildNodes()) {
//     document.getElementById('noItemsAdded').classList.add('disable');
// } else {
//     document.getElementById('noItemsAdded').classList.remove('disable');
// }

/*Delete and toggle the card from/in the DOM*/

allCardsContainer.addEventListener('click', function (event) {
    const element = event.target;
    if (element.dataset.action === "delete") {
        console.log('card deleted');
        (element.closest('div')).remove(); /*remove from DOM*/
    } else if (element.dataset.action === "show") {
        element.nextElementSibling.classList.toggle('toggle'); /*toggle the info*/
    } else if (element.dataset.action === "edit") {
        let editBtn = element;
        editBtn.addEventListener('click', function (event) {
            // ---------
            editBtn.style.display = 'none';
            let saveBtn = editBtn.nextElementSibling;
            saveBtn.style.display = 'inline';
            let card = this.closest('div'); /*card*/
            let allNodes = card.childNodes;
            let idString = allNodes[1].textContent;
            let id = idString.slice(8, 12); /*card id*/
            let content = allNodes[allNodes.length - 1]; /*content*/
            let inputs = content.children; /*inputs*/
            const editedContent = {};
            for (let element of inputs) {
                element.removeAttribute('readonly');
                element.onchange = function () {
                    editedContent[element.name] = element.value;
                }
                saveBtn.onclick = function () {
                    for (let element of inputs) {
                        console.log(element);
                        editedContent[element.name] = element.value;
                        element.setAttribute('readonly', 'true');
                    }
                    Ajax.putRequest(editedContent, id)
                        .then(function (newCard){
                            console.log('this is a new Card', newCard)
                        })
                    // return editedCard
                }
            }
            // -----------------------
        })
    } else if (element.dataset.action === "save") {
        element.style.display = 'none';
        element.previousElementSibling.style.display = 'inline-block';

    }
})
/*Deletes the token when the page is refreshed*/
// window.onbeforeunload = function (e) {
//     localStorage.clear();
// };




